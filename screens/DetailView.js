import React from 'react';
import {Text, View, StyleSheet, Image} from 'react-native';

export default class DetailView extends React.Component {
  render() {
    const {shortDescription, item} = this.props.route.params;
    const images = item['Imágenes'].split(', ');
    console.log(images);
    return (
      <View style={{flex: 1}}>
        <Text style={styles.title}>{item.Nombre}</Text>
        <View style={{flexDirection: 'row'}}>
          <Image source={{uri: images[0]}} style={styles.image} />
          <Image source={{uri: images[1]}} style={styles.image} />
          <Image source={{uri: images[2]}} style={styles.image} />
        </View>
        <View>
          <Text style={styles.container}>{item['Descripción']}</Text>
          <Text style={styles.container}>Marca: {item['Marca']}</Text>
          <Text style={styles.container}>Inventario: {item['Inventario']}</Text>
          <Text style={styles.container}>
            Codigo Sticker: {item['Codigo Sticker']}
          </Text>
          <Text style={styles.container}>Categoria: {item['Categorías']}</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    fontWeight: 'bold',
    fontSize: 20,
    textAlign: 'center',
  },
  image: {width: 115, height: 115, margin: 10, borderRadius: 5},
  container: {
    margin: 10,
    padding: 10,
    backgroundColor: '#fdfdfd',
    borderRadius: 5,
    elevation: 5,
  },
});

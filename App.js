import React from 'react';
import {StyleSheet, View, Text} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import ListView from './screens/ListView';
import DetailView from './screens/DetailView';
//import axios from 'axios';

const Stack = createStackNavigator();

export default class App extends React.Component {
  render() {
    return (
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name="Lista de cubos" component={ListView} />
          <Stack.Screen name="Detalle" component={DetailView} />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}

